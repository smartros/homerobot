package com.isans.smartros.homerobot;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class WorkerService extends Service {
    static public WorkerService theInstance = null;
    @Override
    public void onCreate() {
        theInstance = this;
    }

    static public WorkerService getInstance() {
        return theInstance;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}