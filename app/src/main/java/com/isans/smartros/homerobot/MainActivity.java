package com.isans.smartros.homerobot;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import org.ros.android.smartRosActivity;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends smartRosActivity {
    private int fps = 1;
    private static final String TAG = "MainActivity";
    private HashMap<String, ApplicationInfo> mapPackage = new HashMap<>();
    public MainActivity() {
        super("HomeRobot Node");
    }

    BroadcastReceiver uploadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String result = bundle.getString("SoundRecogResult");
            if(result != null){
                Log.d(TAG, result);
            }
            result = bundle.getString("VideoRecogResult");
            if(result != null){
                Log.d(TAG, result);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task

        Intent intent = new Intent(this, WorkerService.class);
        startService(intent);

        registerReceiver(uploadReceiver, new IntentFilter("com.lge.cic.homerobot.AppIntent.SCRATCH"));

        //get a list of installed apps.
        final PackageManager pm = this.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            String label = (String)packageInfo.loadLabel(pm);
            mapPackage.put(label, packageInfo);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent();
        intent.setAction("com.lge.cic.homerobot.AppIntent.SCRATCH");
        intent.putExtra("ScratchMode", "BEGIN");
        sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(uploadReceiver);

        super.onDestroy();
        Intent intent = new Intent(this, WorkerService.class);
        stopService(intent);

        /*Intent intent = new Intent();
        intent.setAction("com.lge.cic.homerobot.AppIntent.SCRATCH");
        intent.putExtra("ScratchMode", "BEGIN");
        sendBroadcast(intent);
        */
    }

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        if(parts.length == 0) return ;
        switch(parts[1]){
            case "start_all":
                break;
            case "stop_all":
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "hr_motion":
                if(boundary(parts, 2)) {
                    String value = parts[2];
                    String motion = "";
                    switch (value) {
                        case "initialization":
                            motion = "MOTION_INITIALIZATION";
                            break;
                        case "greeting":
                            motion = "MOTION_GRETTING_1";
                            break;
                        case "anticipation":
                            motion = "MOTION_ANTICIPATION";
                            break;
                        case "yes":
                            motion = "MOTION_YES";
                            break;
                        case "no":
                            motion = "MOTION_NO";
                            break;
                        default:
                            break;
                    }
                    Intent intent = new Intent();
                    intent.setAction("com.lge.cic.homerobot.AppIntent.SCRATCH");
                    intent.putExtra("Motion", motion);
                    sendBroadcast(intent);
                }
                break;
            case "hr_recognize":
                if(boundary(parts, 3)) {
                    String value = parts[3];
                    String type = "";
                    switch (value) {
                        case "sound":
                            type = "SoundRecog";
                            break;
                        case "video":
                            type = "VideoRecog";
                            break;
                        default:
                            break;
                    }
                    Intent intent = new Intent();
                    intent.setAction("com.lge.cic.homerobot.AppIntent.SCRATCH");
                    intent.putExtra(type, parts[2].toUpperCase());
                    sendBroadcast(intent);
                }
                break;
            case "hr_start_app":
                if(boundary(parts, 2)){
                    sr_start_package(parts[2]);
                }
                break;
            default :
                break;
        }
    }
    private void sr_start_package(String name) {
        ApplicationInfo app = mapPackage.get(name);
        if (app != null) {

            Intent i = new Intent();
            i.setAction("com.lge.cic.homerobot.AppIntent.SCRATCH");
            i.putExtra("ScratchMode", "END");
            sendBroadcast(i);

            final PackageManager pm = this.getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(app.packageName);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            WorkerService.getInstance().startActivity(intent);
        }
    }
    private boolean boundary(String[] range, int idx){
        return (range.length > idx );
    }
}
